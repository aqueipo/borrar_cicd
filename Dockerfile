from registry.gitlab.com/aqueipo/borrar_cicd/borrarcicd_app:latest

COPY ./ /home/sample_proj/
RUN apt-get install -y libmysqlclient-dev \
&& rm /etc/nginx/sites-available/default \
&& ls /home/sample_proj/ \
&& cp /home/sample_proj/confs/nginx.conf /etc/nginx/sites-available/sample.conf \
&& ln -s /etc/nginx/sites-available/sample.conf /etc/nginx/sites-enabled/sample.conf \
&& pip install -r /home/sample_proj/requirements.txt \
&& echo "service nginx start\n \
uwsgi --ini /home/sample_proj/confs/uwsgi.ini" > /tmp/runscript.exe \
&& chmod +x /tmp/runscript.exe
ENTRYPOINT /tmp/runscript.exe
